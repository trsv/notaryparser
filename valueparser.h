#ifndef VALUEPARSER_H
#define VALUEPARSER_H

#include <QtCore>
#include <QList>

class ValueParser
{
public:
    ValueParser(const QString & raw);
    ValueParser();
public:
    QString parse_regex(const QString & pattern_regex);
    static QString parse_regex(const QString & pattern_regex, const QString & raw);

private:
    const QString m_raw;
    const QString m_raw_simplified;
};

#endif // VALUEPARSER_H
