#include "presenter.h"

#include <QCoreApplication>

static Presenter * presenter = nullptr;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    presenter = new Presenter;

    return a.exec();
}
