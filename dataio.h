#ifndef DATAIO_H
#define DATAIO_H

#include "struct.h"

class DataIO
{
public:
    DataIO();

public:
    static void save_to_xlsx(const QString & path, const QList <Company> & company_list);

private:
    static QString get_header_name_by_column_index(const int & column);

private:
    static const auto COLUMN_COUNT = 15;
};

#endif // DATAIO_H
