#include "httputils.h"

HttpUtils::HttpUtils()
{

}

QNetworkRequest HttpUtils::browserRequest(const QUrl& url, const QNetworkAccessManager::Operation & op)
{
    QNetworkRequest request(url);
    request.setRawHeader("User-Agent" , "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36");


    if (op == QNetworkAccessManager::PostOperation) {
        request.setRawHeader("Accept", "*/*");
        request.setRawHeader("X-Requested-With", "XMLHttpRequest");
    } else {
        request.setRawHeader("Content-Type", "application/x-www-form-urlencoded");
    }

    return request;
}
