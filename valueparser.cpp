#include "valueparser.h"

static QRegularExpression m_regex;

ValueParser::ValueParser(const QString &raw) :
    m_raw(raw),
    m_raw_simplified(raw.simplified())
{

}

ValueParser::ValueParser()
{

}

QString ValueParser::parse_regex(const QString &pattern_regex)
{
    m_regex.setPattern(pattern_regex);

    const auto match = m_regex.match(m_raw_simplified);
    const auto value = match.captured(1);

    return value.trimmed();
}

QString ValueParser::parse_regex(const QString &pattern_regex, const QString &raw)
{
    m_regex.setPattern(pattern_regex);

    const auto match = m_regex.match(raw);
    const auto value = match.captured(1);

    return value.trimmed();
}



