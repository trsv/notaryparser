#include "httpresponse.h"
#include <QFile>

class HttpResponsePrivate : public QSharedData
{
public:
    HttpResponsePrivate():
        QSharedData()
    {}

    HttpResponsePrivate(const HttpResponsePrivate& other)
        : QSharedData(other)
        , isTimeout(false)
        , httpStatus(other.httpStatus)
        , error(other.error)
        , operation(other.operation)
        , request(other.request)
        , rawHeaderParis(other.rawHeaderParis)
        , response(other.response)

    {}

    bool isTimeout = false;
    int httpStatus = 0;

    QNetworkReply::NetworkError error;
    QNetworkAccessManager::Operation operation;

    QNetworkRequest request;
    QList<QNetworkReply::RawHeaderPair> rawHeaderParis;
    QByteArray response;
};

HttpResponse::HttpResponse()
    : d(new HttpResponsePrivate)
{}

HttpResponse::HttpResponse(const HttpResponse &other)
    : d(other.d)
{}

HttpResponse &HttpResponse::operator=(const HttpResponse &other) {
    d = other.d;
    return *this;
}

HttpResponse::~HttpResponse()
{}

QByteArray HttpResponse::getResponse() const
{
    return d->response;
}

void HttpResponse::setResponse(const QByteArray &value)
{
    d->response = value;
}

int HttpResponse::getHttpStatus() const
{
    return d->httpStatus;
}

void HttpResponse::setHttpStatus(int value)
{
    d->httpStatus = value;
}

bool HttpResponse::getIsTimeout() const
{
    return d->isTimeout;
}

void HttpResponse::setIsTimeout(bool value)
{
    d->isTimeout = value;
}

HttpResponse HttpResponse::createFromReply(QNetworkReply *reply)
{
    HttpResponse response;
    response.setError(reply->error());
    response.setOperation(reply->operation());
    response.setRawHeaderParis(reply->rawHeaderPairs());
    response.setRequest(reply->request());

    if (reply->isReadable()) {
        QString str = reply->readAll();
        QString lockNew = reply->header(QNetworkRequest::LocationHeader).toString();
        QFile file("res2.txt");
        file.open(QIODevice::WriteOnly);
        file.write(str.toStdString().c_str());
        file.close();
        QByteArray arr;
        arr.append(str);
        response.setResponse(arr);
        response.setHttpStatus(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt());
    } else if (reply->error() == QNetworkReply::OperationCanceledError) {
        response.setResponse("timeout and abort");
        response.setHttpStatus(-1);
    }

    return response;
}

QNetworkAccessManager::Operation HttpResponse::getOperation() const
{
    return d->operation;
}

void HttpResponse::setOperation(const QNetworkAccessManager::Operation &value)
{
    d->operation = value;
}

QList<QNetworkReply::RawHeaderPair> HttpResponse::getRawHeaderParis() const
{
    return d->rawHeaderParis;
}

void HttpResponse::setRawHeaderParis(const QList<QNetworkReply::RawHeaderPair> &value)
{
    d->rawHeaderParis = value;
}

QNetworkReply::NetworkError HttpResponse::getError() const
{
    return d->error;
}

void HttpResponse::setError(const QNetworkReply::NetworkError &value)
{
    d->error = value;
}

QNetworkRequest HttpResponse::getRequest() const
{
    return d->request;
}

void HttpResponse::setRequest(const QNetworkRequest &value)
{
    d->request = value;
}
