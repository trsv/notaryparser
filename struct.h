#ifndef STRUCT_H
#define STRUCT_H

#include "valueparser.h"

#include <iostream>

#include <QtCore>
#include <QTextDocument>

struct User {

    User() {}
    User(const QJsonObject & json) {
        user_id = json.value("id").toString().toInt();

        certificate_id = json.value("certnum").toString();
        certificate_devision = json.value("certcalc").toString();
        certificate_date = QDateTime::fromString(json.value("certat").toString(), "yyyy-MM-dd hh:mm:ss")
                .date().toString(Qt::LocalDate);

        firstname = json.value("firstname").toString();
        surname = json.value("surname").toString();
        middlename = json.value("middlename").toString();

        registration = json.value("racalc").toString();
    }

    int user_id = -1;

    QString certificate_id;
    QString certificate_devision;
    QString certificate_date;
    QString registration;
    QString firstname;
    QString middlename;
    QString surname;
};

struct Company : User {

    Company() {}
    Company(const User & user) : User(user) {}
    Company(const QByteArray & raw, const User & user) : User(user) {
        const QString html = raw;

        ValueParser parser(html);
        phone_city = parser.parse_regex("Стаціонарний: <\\/div> <div.*?> <a href=\"tel:(.*?)\">");
        phone_mobile = parser.parse_regex("Мобільний: <\\/div> <div.*?> <a href=\"tel:(.*?)\">");
        email = parser.parse_regex("mailto:(.*?)\"");

        address = parser.parse_regex("Адреса:<\\/div> <div class=\"text-info col-md-9\"> (.*?)<\\/div>");
        region = parser.parse_regex("\\d+,(.*?),");

        // todo: description
        // todo: facebook
        // todo: site
    }

    QString get_value_by_column_index(const int & column) const {
        switch (column) {
        case 2: return this->category;
        case 3: return this->source_url;
        case 4: return this->profile_url;
        case 5: {
            if (has_fullname) {
                return this->fullname;
            } else {
                return QString("%1 %2 %3").arg(this->surname, this->firstname, this->middlename);
            }
        }
        case 6: return this->phone_mobile;
        case 7: return this->phone_city;
        case 8: return this->email;
        case 9: return this->facebook;
        case 10: return this->site;
        case 11: return this->region;
        case 12: return this->address;
        case 13: return  this->description;
        case 14: return this->certificate_id;
        case 15: return this->certificate_date;
        default: return QString();
        }
    }

    bool has_fullname = false;

    QString category;
    QString source_url;
    QString profile_url;

    QString phone_mobile;
    QString phone_city;
    QString email;
    QString facebook;
    QString site;
    QString region;
    QString address;
    QString description;
    QString fullname;
};

struct InfoStruct
{

    Company toCompany(const QString & region) const {
        Company company;
        QTextDocument text;

        company.has_fullname = true;

        // декодируем html-символы
        text.setHtml(this->m_address);
        const auto address = text.toPlainText().trimmed();

        text.setHtml(this->m_contacts);
        const auto contacts = text.toPlainText().trimmed();

        text.setHtml(this->m_name);
        const auto name = text.toPlainText().trimmed();

        company.address = contacts;
        const auto contact_index = contacts.indexOf(", тел");
        if (contact_index != -1) {
            company.address = company.address.left(contact_index);
        }

        if (this->m_name.contains("<br/>")) {
            company.fullname = ValueParser::parse_regex("(.*?)\\n", name);
            company.certificate_id = ValueParser::parse_regex("№(.*?)$", name);
            company.phone_city = ValueParser::parse_regex("тел\\.:(.*?)$", contacts);
        } else {
            company.fullname = name;
        }

        company.category = "нотариус";
        company.source_url = "http://ern.minjust.gov.ua";
        company.region = region.simplified().trimmed();
        company.address = company.address.simplified().trimmed();

        return company;
    }

    static QList <InfoStruct> from_page(const QByteArray & page) {

        static const QString key = ",name:\"";

        QList <InfoStruct> list;

        if (page.indexOf(key) == -1) {
            return list;
        }

        int indexStart = page.indexOf(key) + key.length();
        int indexEnd = 0;
        QString strName = ",name:\"", strAddress = ",address:\"", strContact = ",contacts:\"";
        if(key == strName)
        {

            indexStart = page.indexOf(strName) + strName.length();
            while(true)
            {
                InfoStruct infoBuf;
                indexEnd = page.indexOf("\"", indexStart);
                infoBuf.m_name = page.mid(indexStart, indexEnd - indexStart);

                indexEnd += 2;
                indexStart = page.indexOf(strAddress, indexEnd) + strAddress.length();
                indexEnd = page.indexOf("\"", indexStart);
                infoBuf.m_address = page.mid(indexStart, indexEnd - indexStart);


                indexEnd += 2;
                indexStart = page.indexOf(strContact, indexEnd) + strContact.length();
                indexEnd = page.indexOf("\"", indexStart);
                infoBuf.m_contacts = page.mid(indexStart, indexEnd - indexStart);

                indexStart = page.indexOf(strName, indexEnd) + strName.length();
                list.push_back(infoBuf);
                if((indexStart - strName.length()) == -1)
                    break;
            }
        }

        return list;
    }

    inline bool operator==(const InfoStruct & other) const {
        return this->m_name == other.m_name &&
                this->m_address == other.m_address &&
                this->m_contacts == other.m_contacts;
    }

    QString m_name;
    QString m_address;
    QString m_contacts;
};

#endif // STRUCT_H
