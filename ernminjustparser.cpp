#include "ernminjustparser.h"
#include "httputils.h"
#include "dataio.h"

#include <QtCore>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

ErnMinjustParser::ErnMinjustParser()
{
    QObject::connect(&m_network, &QNetworkAccessManager::finished, &m_loop, &QEventLoop::quit);

    {
        QUrl url("http://ern.minjust.gov.ua/pages/notary.aspx?search=atu");

        const auto request = HttpUtils::browserRequest(url);
        const auto response = handle_response(QNetworkAccessManager::GetOperation, request);

        m_general_search_page = response.getResponse();

        __VIEWSTATE = get_aspnet_value("__VIEWSTATE");
        __VIEWSTATEGENERATOR = get_aspnet_value("__VIEWSTATEGENERATOR");
        ctl00DefaultContentASPxGridView1CallbackState = get_aspnet_value("ctl00$DefaultContent$ASPxGridView1$CallbackState");
    }

    {
        QUrl url("http://ern.minjust.gov.ua/pages/AtuDictionariesService.asmx/GetDropDownCategories");

        auto request = HttpUtils::browserRequest(url, QNetworkAccessManager::PostOperation);
        request.setRawHeader("Content-Type", "application/json; charset=UTF-8");
        const auto response = handle_response(
                    QNetworkAccessManager::PostOperation,
                    request,
                    "{\"knownCategoryValues\":\"\",\"category\":\"atu1\"}");

        m_city_list = parse_city_list(response.getResponse());
    }

    {

        QList <Company> company_list;
        for(auto iter = m_city_list.cbegin(); iter != m_city_list.cend(); ++iter)
        {
            QUrl url("http://ern.minjust.gov.ua/pages/AtuDictionariesService.asmx/GetDropDownCategories");
            QByteArray arr;
            arr.append("{\"knownCategoryValues\":\"atu1:");
            arr.append(iter.value().mid(0, iter.value().indexOf("|")));
            arr.append("|Region\",\"category\":\"atu2\"}");
            QNetworkRequest request = HttpUtils::browserRequest(url, QNetworkAccessManager::PostOperation);

            request.setRawHeader("Content-Type", "application/json; charset=UTF-8");
            const auto response = handle_response(
                        QNetworkAccessManager::PostOperation,
                        request,
                        arr );
            m_area_list = parse_city_list(response.getResponse());

            qDebug() << "Parse region:" << iter.key();
            const auto list = parsingAll(iter.value(), iter.key());
            company_list << list;
        }

        DataIO::save_to_xlsx("notary_out.xlsx", company_list);
    }

}

ErnMinjustParser::~ErnMinjustParser()
{

}



QList<Company> ErnMinjustParser::parse(const QMap<QString, QStringList> &list)
{
    QList <Company> result;
    for (auto it = list.cbegin(); it != list.cend(); ++it) {
        const auto notary_list_by_city = this->parse(it.key(),it.value());
        result << notary_list_by_city;
    }

    return result;
}

QList<Company> ErnMinjustParser::parse(const QString &city, const QStringList &street_list)
{
    QList <Company> result;
    for (const auto & street : street_list) {
        const auto response = send_search_request(city, street);
        const auto list = parse_user_list(response.getResponse());

        result << list;
    }

    return result;
}

QList<Company> ErnMinjustParser::parse_user_list(const QByteArray &response)
{
    return  QList<Company>();
}

QByteArray ErnMinjustParser::build_search_query(const QString &city, const QString &street)
{
    QUrlQuery query;
    query.addQueryItem("ctl00$scm", "ctl00$DefaultContent$upSearchFilter|ctl00$DefaultContent$ctl01$ImageButton6");
    query.addQueryItem("__EVENTTARGET", "");
    query.addQueryItem("__EVENTARGUMENT", "");
    query.addQueryItem("__VIEWSTATE", __VIEWSTATE);
    query.addQueryItem("__VIEWSTATEGENERATOR", __VIEWSTATEGENERATOR);
    query.addQueryItem("ctl00$DefaultContent$ASPxGridView1$DXSelInput", "");
    query.addQueryItem("ctl00$DefaultContent$ASPxGridView1$CallbackState", ctl00DefaultContentASPxGridView1CallbackState);
    query.addQueryItem("hfMap", "");
    query.addQueryItem("ctl00$DefaultContent$ctl01$cblOrgType$0", "914");
    query.addQueryItem("ctl00$DefaultContent$ctl01$cblOrgType$1", "917");
    query.addQueryItem("ctl00$DefaultContent$ctl01$ddAtu1", city);
    query.addQueryItem("ctl00$DefaultContent$ctl01$cddAtu1_ClientState", city);
    query.addQueryItem("ctl00$DefaultContent$ctl01$ddAtu2", "");
    query.addQueryItem("ctl00$DefaultContent$ctl01$cddAtu2_ClientState", ":::");
    query.addQueryItem("ctl00$DefaultContent$ctl01$ddAtu3", "");
    query.addQueryItem("ctl00$DefaultContent$ctl01$cddAtu3_ClientState", ":::");
    query.addQueryItem("ctl00$DefaultContent$ctl01$tbNotaryAddress", street);
    query.addQueryItem("hiddenInputToUpdateATBuffer_CommonToolkitScripts", "1");
    query.addQueryItem("DXScript", "1_23,1_30,1_42,3_6");
    query.addQueryItem("__ASYNCPOST", "true");
    query.addQueryItem("ctl00$DefaultContent$ctl01$ImageButton6.x", "38");
    query.addQueryItem("ctl00$DefaultContent$ctl01$ImageButton6.y", "18");

    auto result = QUrl::toPercentEncoding(query.toString());
    result.append("&RegexHiddenField_cvCertificateNum=" + RegexHiddenField_cvCertificateNum);
    result.append("&RegexHiddenField_cvFName=" + RegexHiddenField_cvFName);
    result.append("&RegexHiddenField_cvName" + RegexHiddenField_cvName);
    result.append("&RegexHiddenField_cvSName" + RegexHiddenField_cvSName);

    return result;
}

HttpResponse ErnMinjustParser::send_search_request(const QString &city, const QString &street)
{
    const auto query = build_search_query(city, street);
    QUrl url("http://ern.minjust.gov.ua/pages/notary.aspx?search=atu");

    const auto request = HttpUtils::browserRequest(url);
    const auto response = handle_response(QNetworkAccessManager::PostOperation, request, query);

    return response;
}

HttpResponse ErnMinjustParser::handle_response(const QNetworkAccessManager::Operation &op, const QNetworkRequest &request, const QByteArray &args)
{
    auto reply = (op == QNetworkAccessManager::GetOperation) ?
              m_network.get(request)
              : m_network.post(request, args);

    m_loop.exec();

    const auto response = HttpResponse::createFromReply(reply);
    reply->deleteLater();

    return response;
}

QString ErnMinjustParser::get_aspnet_value(const QString &name)
{
    static QRegularExpression regex;
    static QString pattern = "name=\"%1\" id=\".*?\" value=\"(.*?)\"";

    auto valid_name = name;
    valid_name = valid_name.replace("$", "\\$");

    regex.setPattern(pattern.arg(valid_name));

    return regex.match(m_general_search_page).captured(1);
}

QMap<QString, QString> ErnMinjustParser::parse_city_list(const QByteArray &response)
{
    const auto list = QJsonDocument::fromJson(response).object().value("d").toArray();

    QMap<QString, QString> city_list;
    for (const auto & city_raw : list) {
        const auto city = city_raw.toObject();

        const auto name = city.value("name").toString();
        const auto value = city.value("value").toString();

        city_list.insert(name, value);
    }

    return  city_list;
}

QList <Company> ErnMinjustParser::parsingAll(const QString & iter1, const QString &iter11)
{
    QList <Company> company_list;

    for(auto iter2 = m_area_list.cbegin(); iter2 != m_area_list.cend(); ++iter2)
    {
        for(int i = 0; i < 10;i++)
        {
            QUrlQuery query;
            query.addQueryItem("ctl00$scm", "ctl00$DefaultContent$upSearchFilter|ctl00$DefaultContent$ctl01$ImageButton6");
            query.addQueryItem("ctl00$DefaultContent$ASPxGridView1$DXSelInput", "");
            query.addQueryItem("ctl00$DefaultContent$ASPxGridView1$CallbackState", ctl00DefaultContentASPxGridView1CallbackState);
            query.addQueryItem("hfMap", "");
            query.addQueryItem("ctl00$DefaultContent$ctl01$cblOrgType$0", "914");
            query.addQueryItem("ctl00$DefaultContent$ctl01$cblOrgType$1", "917");
            query.addQueryItem("ctl00$DefaultContent$ctl01$ddAtu1", iter1);
            query.addQueryItem("ctl00$DefaultContent$ctl01$cddAtu1_ClientState", iter1 + ":::" + iter11);
            query.addQueryItem("ctl00$DefaultContent$ctl01$ddAtu2", iter2.value());
            query.addQueryItem("ctl00$DefaultContent$ctl01$cddAtu2_ClientState", iter2.value() + ":::" + iter2.key());
            query.addQueryItem("ctl00$DefaultContent$ctl01$ddAtu3", "");
            query.addQueryItem("ctl00$DefaultContent$ctl01$cddAtu3_ClientState", ":::");
            query.addQueryItem("ctl00$DefaultContent$ctl01$tbNotaryAddress", "");
            query.addQueryItem("hiddenInputToUpdateATBuffer_CommonToolkitScripts", "0");
            query.addQueryItem("DXScript", "2_21,2_28,1_23,1_30,1_42,3_6");
            query.addQueryItem("__EVENTTARGET", "");
            query.addQueryItem("__EVENTARGUMENT", "");
            query.addQueryItem("__VIEWSTATE", "%2FwEPDwUIMjIyNzA2OTgPZBYCZg9kFgICBQ9kFgQCAQ88KwANAgAPFgIeC18hRGF0YUJvdW5kZ2QMFCsAAwUHMDowLDA6MRQrAAIWDh4EVGV4dAUO0JPQvtC70L7QstC90LAeBVZhbHVlBQ7Qk9C%2B0LvQvtCy0L3QsB4LTmF2aWdhdGVVcmwFFX4vcGFnZXMvZGVmYXVsdC5hc3B4Px4HRW5hYmxlZGceClNlbGVjdGFibGVnHghEYXRhUGF0aAUFUGFnZXMeCURhdGFCb3VuZGcUKwAEBQswOjAsMDoxLDA6MhQrAAIWDh8BBUPQn9C%2B0YjRg9C6INC00LXRgNC20LDQstC90LjRhSDQvdC%2B0YLQsNGA0ZbQsNC70YzQvdC40YUg0LrQvtC90YLQvtGAHwIFQ9Cf0L7RiNGD0Log0LTQtdGA0LbQsNCy0L3QuNGFINC90L7RgtCw0YDRltCw0LvRjNC90LjRhSDQutC%2B0L3RgtC%2B0YAfAwUdfi9wYWdlcy9ub3RhcnkuYXNweD9zZWFyY2g9aWQfBGcfBWcfBgUQTm90YXJ5U2VhcmNoQnlJZB8HZ2QUKwACFg4fAQUf0J%2FQvtGI0YPQuiDQvdC%2B0YLQsNGA0ZbRg9GB0ZbQsh8CBR%2FQn9C%2B0YjRg9C6INC90L7RgtCw0YDRltGD0YHRltCyHwMFH34vcGFnZXMvbm90YXJ5LmFzcHg%2Fc2VhcmNoPW5hbWUfBGcfBWcfBgUSTm90YXJ5U2VhcmNoQnlOYW1lHwdnZBQrAAIWDh8BBR7Qn9C%2B0YjRg9C6INC30LAg0LDQtNGA0LXRgdC%2B0Y4fAgUe0J%2FQvtGI0YPQuiDQt9CwINCw0LTRgNC10YHQvtGOHwMFHn4vcGFnZXMvbm90YXJ5LmFzcHg%2Fc2VhcmNoPWF0dR8EZx8FZx8GBRFOb3RhcnlTZWFyY2hCeUF0dR8HZ2QUKwACFg4fAQUQ0JTQvtC%2F0L7QvNC%2B0LPQsB8CBRDQlNC%2B0L%2FQvtC80L7Qs9CwHwMFEn4vcGFnZXMvaGVscC5hc3B4Px8EZx8FZx8GBQRIZWxwHwdnZBYKZg9kFgICAQ8PFgIeD0NvbW1hbmRBcmd1bWVudAUVfi9wYWdlcy9kZWZhdWx0LmFzcHg%2FZBYCZg8VAQ7Qk9C%2B0LvQvtCy0L3QsGQCAQ9kFgICAQ8PFgIfCAUdfi9wYWdlcy9ub3RhcnkuYXNweD9zZWFyY2g9aWRkFgJmDxUBQ9Cf0L7RiNGD0Log0LTQtdGA0LbQsNCy0L3QuNGFINC90L7RgtCw0YDRltCw0LvRjNC90LjRhSDQutC%2B0L3RgtC%2B0YBkAgIPZBYCAgEPDxYCHwgFH34vcGFnZXMvbm90YXJ5LmFzcHg%2Fc2VhcmNoPW5hbWVkFgJmDxUBH9Cf0L7RiNGD0Log0L3QvtGC0LDRgNGW0YPRgdGW0LJkAgMPZBYCAgEPDxYCHwgFHn4vcGFnZXMvbm90YXJ5LmFzcHg%2Fc2VhcmNoPWF0dWQWAmYPFQEe0J%2FQvtGI0YPQuiDQt9CwINCw0LTRgNC10YHQvtGOZAIED2QWAgIBDw8WAh8IBRJ%2BL3BhZ2VzL2hlbHAuYXNweD9kFgJmDxUBENCU0L7Qv9C%2B0LzQvtCz0LBkAgQPZBYEAgEPZBYCZg9kFgQCAw8WAh4Fc3R5bGUFIXRleHQtYWxpZ246anVzdGlmeTtjb2xvcjojMDA1NWNjO2QCBQ9kFgICAQ9kFgICAQ9kFgZmD2QWDAIFDw8WAh4JTWF4TGVuZ3RoAgVkZAIHDw8WBB4MRXJyb3JNZXNzYWdlBYgBPGltZyBzcmM9Jy4uL0FwcF9UaGVtZXMvRGVmYXVsdC9pbWFnZXMvZXJyb3Jwcm92aWRlci5naWYnIHRpdGxlID0gJ9Cf0L7Qu9C1INC80L7QttC1INC80ZbRgdGC0LjRgtC4INC00L4gNSDRhtC40YTRgCcgY2xhc3M9J0Vycm9ySW1hZ2UnPh4YQ2xpZW50VmFsaWRhdGlvbkZ1bmN0aW9uBSBSZWdleFZhbGlkYXRpb25fY3ZDZXJ0aWZpY2F0ZU51bWRkAg8PDxYEHwsF5AE8aW1nIHNyYz0nLi4vQXBwX1RoZW1lcy9EZWZhdWx0L2ltYWdlcy9lcnJvcnByb3ZpZGVyLmdpZicgdGl0bGUgPSAn0J3QtdCy0ZbRgNC90LjQuSDRhNC%2B0YDQvNCw0YIg0YLQtdC60YHRgtGDOiDRgdC40LzQstC%2B0LvQuCAiJSIg0LDQsdC%2BICJfIiDQvdC10LTQvtC%2F0YPRgdGC0LjQvNGWINC90LAg0L%2FQtdGA0YjQuNGFINC00LLQvtGFINC%2F0L7Qt9C40YbRltGP0YUnIGNsYXNzPSdFcnJvckltYWdlJz4fDAUXUmVnZXhWYWxpZGF0aW9uX2N2Rk5hbWVkZAIXDw8WBB8LBeQBPGltZyBzcmM9Jy4uL0FwcF9UaGVtZXMvRGVmYXVsdC9pbWFnZXMvZXJyb3Jwcm92aWRlci5naWYnIHRpdGxlID0gJ9Cd0LXQstGW0YDQvdC40Lkg0YTQvtGA0LzQsNGCINGC0LXQutGB0YLRgzog0YHQuNC80LLQvtC70LggIiUiINCw0LHQviAiXyIg0L3QtdC00L7Qv9GD0YHRgtC40LzRliDQvdCwINC%2F0LXRgNGI0LjRhSDQtNCy0L7RhSDQv9C%2B0LfQuNGG0ZbRj9GFJyBjbGFzcz0nRXJyb3JJbWFnZSc%2BHwwFFlJlZ2V4VmFsaWRhdGlvbl9jdk5hbWVkZAIdDw8WBB8LBeQBPGltZyBzcmM9Jy4uL0FwcF9UaGVtZXMvRGVmYXVsdC9pbWFnZXMvZXJyb3Jwcm92aWRlci5naWYnIHRpdGxlID0gJ9Cd0LXQstGW0YDQvdC40Lkg0YTQvtGA0LzQsNGCINGC0LXQutGB0YLRgzog0YHQuNC80LLQvtC70LggIiUiINCw0LHQviAiXyIg0L3QtdC00L7Qv9GD0YHRgtC40LzRliDQvdCwINC%2F0LXRgNGI0LjRhSDQtNCy0L7RhSDQv9C%2B0LfQuNGG0ZbRj9GFJyBjbGFzcz0nRXJyb3JJbWFnZSc%2BHwwFF1JlZ2V4VmFsaWRhdGlvbl9jdlNOYW1lZGQCJQ8PFgIfCwWsATxpbWcgc3JjPScuLi9BcHBfVGhlbWVzL0RlZmF1bHQvaW1hZ2VzL2Vycm9ycHJvdmlkZXIuZ2lmJyB0aXRsZSA9ICfQndC10L7QsdGF0ZbQtNC90L4g0LLQutCw0LfQsNGC0Lgg0YXQvtGH0LAg0LEg0L7QtNC40L0g0L%2FQsNGA0LDQvNC10YLRgCDQv9C%2B0YjRg9C60YMnIGNsYXNzPSdFcnJvckltYWdlJz5kZAIBD2QWBgIFDxBkEBUBABUBABQrAwFnFgFmZAIJDw8WAh8LBYYBPGltZyBzcmM9Jy4uL0FwcF9UaGVtZXMvRGVmYXVsdC9pbWFnZXMvZXJyb3Jwcm92aWRlci5naWYnIHRpdGxlID0gJ9Cd0LXQvtCx0YXRltC00L3QviDQstC40LHRgNCw0YLQuCDRgNC10LPRltC%2B0L0nIGNsYXNzPSdFcnJvckltYWdlJz5kZAIPDxBkEBUBABUBABQrAwFnFgFmZAICD2QWDgIDDxAPFgIfAGdkEBUCNtCU0LXRgNC20LDQstC90LAg0L3QvtGC0LDRgNGW0LDQu9GM0L3QsCDQutC%2B0L3RgtC%2B0YDQsCPQn9GA0LjQstCw0YLQvdC40Lkg0L3QvtGC0LDRgNGW0YPRgRUCAzkxNAM5MTcUKwMCZ2dkZAIHDxBkEBUBABUBABQrAwFnZGQCCw8PFgIfCwWGATxpbWcgc3JjPScuLi9BcHBfVGhlbWVzL0RlZmF1bHQvaW1hZ2VzL2Vycm9ycHJvdmlkZXIuZ2lmJyB0aXRsZSA9ICfQndC10L7QsdGF0ZbQtNC90L4g0LLQuNCx0YDQsNGC0Lgg0YDQtdCz0ZbQvtC9JyBjbGFzcz0nRXJyb3JJbWFnZSc%2BZGQCEQ8QZBAVAQAVAQAUKwMBZ2RkAhUPDxYCHwsFhAE8aW1nIHNyYz0nLi4vQXBwX1RoZW1lcy9EZWZhdWx0L2ltYWdlcy9lcnJvcnByb3ZpZGVyLmdpZicgdGl0bGUgPSAn0J3QtdC%2B0LHRhdGW0LTQvdC%2BINCy0LjQsdGA0LDRgtC4INGA0LDQudC%2B0L0nIGNsYXNzPSdFcnJvckltYWdlJz5kZAIbDxBkEBUBABUBABQrAwFnZGQCIw8PFgIfCwWGATxpbWcgc3JjPScuLi9BcHBfVGhlbWVzL0RlZmF1bHQvaW1hZ2VzL2Vycm9ycHJvdmlkZXIuZ2lmJyB0aXRsZSA9ICfQndC10L7QsdGF0ZbQtNC90L4g0LLQutCw0LfQsNGC0Lgg0LLRg9C70LjRhtGOJyBjbGFzcz0nRXJyb3JJbWFnZSc%2BZGQCAw9kFgJmD2QWAgIBD2QWAgIBD2QWBGYPZBYCAgIPZBYCAgEPDxYCHwEFCjAxLjEwLjIwMThkZAIBD2QWAmYPZBYGAgEPZBYCAgMPPCsAFgIADxYEHg1DYWxsYmFja1N0YXRlBcgBL3dFV0JCNEVSR0YwWVFVc1FVRkJRVUZCUVVGQlFVRkJRVUZCUVVGQlFVRkJRVUZCUVVGQlFVRkJRVUZCUVVGQlFVRkJTRUZCWTBFZUJWTjBZWFJsQlZSQ2QxRklRVUZKUWtKM1JVTkJVV05EUVdkRlNFRjNTVUpDZDBGSVFVRmpRVUozUVVOQlFXSXZMM2RyUTBORmJHdFVNa3B4V2xkT01FTlJTVUZCWjBGRVFuZFJRMEZCWTBGQlowVklRVUZKUWtKM1FUMD0eDl8hVXNlVmlld1N0YXRlZ2QGD2QQFgRmAgECAgIDFgQ8KwAKAQAWAh4PQ29sVmlzaWJsZUluZGV4ZjwrAAoBABYCHw8CATwrAAoBABYCHw8CAjwrAAoBABYCHw8CAw8WBAIBAgECAQIBFgEFlQFEZXZFeHByZXNzLldlYi5BU1B4R3JpZFZpZXcuR3JpZFZpZXdEYXRhQ29sdW1uLCBEZXZFeHByZXNzLldlYi5BU1B4R3JpZFZpZXcudjEwLjEsIFZlcnNpb249MTAuMS42LjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49Yjg4ZDE3NTRkNzAwZTQ5YWQCAw9kFgICAQ88KwAWAgAPFgQfDQXoAS93RVdCQjRFUkdGMFlRVXNRVUZCUVVGQlFVRkJRVUZCUVVGQlFVRkJRVUZCUVVGQlFVRkJRVUZCUVVGQlFVRkJRVUZCU0VGQlkwRWVCVk4wWVhSbEJXeENkMmRJUVVGSlFrSjNSVU5CVVdORFFXZEZTRUYzU1VKQ2QxRkRRVkZqUmtGblJVaENaMGxDUW5kalEwRlJZMEZDZDBGSVFVRmpRVUZuUVVjdkx6aEtRV2R3U2xwRlZuUmpSM2gyWlZkV2JFTlJTVUZCWjBGRVFuZFJRMEZCWTBGQlowVklRVUZKUWtKM1FUMD0fDmdkBg9kEBYIZgIBAgICAwIEAgUCBgIHFgg8KwAKAQAWAh8PZjwrAAoBABYCHw8CATwrAAoBABYCHw8CAjwrAAoBABYCHw8CAzwrAAoBABYCHw8CBDwrAAoBABYCHw8CBTwrAAoBABYCHw8CBjwrAAoBABYCHw8CBw8WCAIBAgECAQIBAgECAQIBAgEWAQWVAURldkV4cHJlc3MuV2ViLkFTUHhHcmlkVmlldy5HcmlkVmlld0RhdGFDb2x1bW4sIERldkV4cHJlc3MuV2ViLkFTUHhHcmlkVmlldy52MTAuMSwgVmVyc2lvbj0xMC4xLjYuMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj1iODhkMTc1NGQ3MDBlNDlhZAIFD2QWAgIBDzwrABYCAA8WBB8NBdgBL3dFV0JCNEVSR0YwWVFVc1FVRkJRVUZCUVVGQlFVRkJRVUZCUVVGQlFVRkJRVUZCUVVGQlFVRkJRVUZCUVVGQlFVRkJTRUZCWTBFZUJWTjBZWFJsQldCQ2QxbElRVUZKUWtKM1JVTkJVV05HUVdkQlNFRm5TVUpDZDAxRFFWRmpSVUZuUlVoQlFXTkJRbmRCU0VGQlNVRkNkaTh2UTFGSlNWTlhVbEJaYlhCc1dUTlJTa0ZuUVVOQlFVMUlRa0ZKUVVKM1FVTkJVV05CUVdkRlNFRkJQVDA9Hw5nZAYPZBAWBWYCAQIDAgQCBRYFPCsACgEAFgIfD2Y8KwAKAQAWAh8PAgE8KwAKAQAWAh8PAgI8KwAKAQAWAh8PAgM8KwAKAQAWAh8PAgQPFgUCAQIBAgECAQIBFgEFlQFEZXZFeHByZXNzLldlYi5BU1B4R3JpZFZpZXcuR3JpZFZpZXdEYXRhQ29sdW1uLCBEZXZFeHByZXNzLldlYi5BU1B4R3JpZFZpZXcudjEwLjEsIFZlcnNpb249MTAuMS42LjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49Yjg4ZDE3NTRkNzAwZTQ5YWQYAgUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgUFImN0bDAwJERlZmF1bHRDb250ZW50JEFTUHhHcmlkVmlldzEFJ2N0bDAwJERlZmF1bHRDb250ZW50JGN0bDAxJGNibE9yZ1R5cGUkMAUnY3RsMDAkRGVmYXVsdENvbnRlbnQkY3RsMDEkY2JsT3JnVHlwZSQxBSdjdGwwMCREZWZhdWx0Q29udGVudCRjdGwwMSRjYmxPcmdUeXBlJDEFJ2N0bDAwJERlZmF1bHRDb250ZW50JGN0bDAxJEltYWdlQnV0dG9uNgUkY3RsMDAkRGVmYXVsdENvbnRlbnQkY3RsMDEkbXZGaWx0ZXJzDw9kAgJk7uuQXVvCKQQBTleOKsKG3k42AeeqdhB1BzuCVpuIJks%3D");
            query.addQueryItem("__VIEWSTATEGENERATOR", __VIEWSTATEGENERATOR);
            query.addQueryItem("__ASYNCPOST", "true");
            query.addQueryItem("ctl00$DefaultContent$ctl01$ImageButton6.x", "47");
            query.addQueryItem("ctl00$DefaultContent$ctl01$ImageButton6.y", "23");
            query.addQueryItem("__CALLBACKID", "ctl00$DefaultContent$ctlResult$gvResultByAtu$gvResult");
            query.addQueryItem("__CALLBACKPARAM", "c0:GB|20;12|PAGERONCLICK3|PN" + QString::number(i) +";");

            QByteArray result;
            result.append(query.toString());
            result.append("&RegexHiddenField_cvCertificateNum=" + RegexHiddenField_cvCertificateNum);
            result.append("&RegexHiddenField_cvFName=" + RegexHiddenField_cvFName);
            result.append("&RegexHiddenField_cvName=" + RegexHiddenField_cvName);
            result.append("&RegexHiddenField_cvSName=" + RegexHiddenField_cvSName);
//            qDebug() << result;
            QUrl url("http://ern.minjust.gov.ua/pages/notary.aspx?search=atu");

            QNetworkRequest request2;
            request2.setUrl(url);
            request2.setRawHeader("Host", url.host().toStdString().c_str());
            request2.setRawHeader("Connection", "keep-alive");
            request2.setRawHeader("Origin", "http://ern.minjust.gov.ua");
            request2.setRawHeader("X-Requested-With", "XMLHttpRequest");
            request2.setRawHeader("Cache-Control", "no-cache");
            request2.setRawHeader("X-MicrosoftAjax", "Delta=true");
            request2.setRawHeader("User-Agent" , "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36");
            request2.setRawHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            request2.setRawHeader("Content-Length", QString::number(result.length()).toStdString().c_str());
            request2.setRawHeader("Accept", "*/*");
            request2.setRawHeader("Referer", "http://ern.minjust.gov.ua/pages/notary.aspx?search=atu");
            request2.setRawHeader("Accept-Encoding", "gzip, deflate");
            request2.setRawHeader("Accept-Language", "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7");
            request2.setAttribute(QNetworkRequest::RedirectionTargetAttribute, true);
            const auto response = handle_response(
                        QNetworkAccessManager::PostOperation,
                        request2,
                        result );

            const auto info_on_page_list = InfoStruct::from_page(response.getResponse());

            if (m_last_info_list == info_on_page_list) {
                break;
            } else {
                m_last_info_list = info_on_page_list;
            }

//            QFile file("res.txt");
//            file.open(QIODevice::WriteOnly);
//            file.write(response.getResponse());
//            file.close();

            if (info_on_page_list.isEmpty()) {
                break;
            }

            for (const auto & info : info_on_page_list) {
                company_list << info.toCompany(iter11);
            }

            if (info_on_page_list.size() < 10) {
                break;
            }
        }
    }

    return company_list;
}
