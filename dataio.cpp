#include "dataio.h"

#include "xlsxdocument.h"

DataIO::DataIO()
{

}

void DataIO::save_to_xlsx(const QString &path, const QList<Company> &company_list)
{
    QFileInfo info(path);
    const auto absolute_path = info.absolutePath();
    QDir().mkpath(absolute_path);

    QXlsx::Document document(path);

    int row = 1;

    for (int column = 1; column <= COLUMN_COUNT; ++column) {
        document.write(row, column, get_header_name_by_column_index(column));
    }


    for (const auto & company : company_list) {

        ++row;

        document.write(row, 1, row - 1);

        for (int column = 2; column <= COLUMN_COUNT; ++column) {
            document.write(row, column, company.get_value_by_column_index(column));
        }

        qDebug() << "[DataIO] Saved" << row << "row to" << path;
    }

    document.save();
    document.deleteLater();
}

QString DataIO::get_header_name_by_column_index(const int &column)
{
    switch (column) {
    case 1: return "№";
    case 2: return "Категория";
    case 3: return "Сайт-источник";
    case 4: return "Карточка компании";
    case 5: return "Юридическое название";
    case 6: return "Моб. телефон";
    case 7: return "Город. телефон";
    case 8: return "Почта";
    case 9: return "Фейсбук";
    case 10: return "Сайт компании";
    case 11: return "Регион";
    case 12: return "Адрес";
    case 13: return "Описание компании";
    case 14: return "№ свидетельства";
    case 15: return "Дата выдачи свидетельства";
    default: return QString();
    }
}
