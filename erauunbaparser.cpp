#include "erauunbaparser.h"
#include "httputils.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

ErauUnbaParser::ErauUnbaParser() :
    m_USER_ON_PAGE_LIMIT(100)
{
    QObject::connect(&m_network, &QNetworkAccessManager::finished, &m_loop, &QEventLoop::quit);
}

ErauUnbaParser::~ErauUnbaParser()
{

}

QList <User> ErauUnbaParser::parse()
{
    const auto user_list_size = get_user_list_size();
    const auto page_count = (user_list_size % m_USER_ON_PAGE_LIMIT) ?
                user_list_size / m_USER_ON_PAGE_LIMIT + 1
              : user_list_size / m_USER_ON_PAGE_LIMIT;

    QList <User> user_list;
    for (int page = 0; page < page_count; ++page) {
        const auto offset = page * m_USER_ON_PAGE_LIMIT;
        const auto response = send_search_request(m_USER_ON_PAGE_LIMIT, offset);
        const auto list = parse_user_list(response.getResponse());
        user_list << list;
    }

    QFile file("user_list.json");
    file.open(QIODevice::WriteOnly);
    file.write(QJsonDocument(m_user_list).toJson(QJsonDocument::Compact));
    file.close();

    qDebug() << "end";

    return user_list;
}

QList<Company> ErauUnbaParser::parse_additional_data(const QList<User> & user_list)
{
    QList <Company> company_list;
    for (const auto & user : user_list) {
//        const auto response = send_profile_request(user.user_id);

//        Company company(response.getResponse(), user);

        QFile file(QString("users/%1.txt").arg(user.user_id));
        file.open(QIODevice::ReadOnly);
        const auto content = file.readAll();
        file.close();

        Company company(content, user);
        company.category = "адвокат";
        company.source_url = "http://erau.unba.org.ua";
        company.profile_url = QString("http://erau.unba.org.ua/profile/%1").arg(user.user_id);

        company_list << company;
    }

    return company_list;
}

int ErauUnbaParser::get_user_list_size()
{
    static const auto user_list_size = get_user_list_size_request();
    return user_list_size;
}

int ErauUnbaParser::get_user_list_size_request()
{
    const auto response = send_search_request(0, 0);
    const auto json = QJsonDocument::fromJson(response.getResponse()).object();
    const auto user_list_size = json.value("total").toString().toInt();

    return user_list_size;
}

QUrlQuery ErauUnbaParser::build_search_query(const int & limit, const int & offset) const
{
    QUrlQuery query;
//    query.addQueryItem("limit", QString::number(limit));
//    query.addQueryItem("offset", QString::number(offset));
//    query.addQueryItem("order[surname]", "ASC");
//    query.addQueryItem("addation[probono]", "0");
//    query.addQueryItem("foreigner", "0");

    return query;
}

QList<User> ErauUnbaParser::parse_user_list(const QByteArray &response)
{
    const auto json = QJsonDocument::fromJson(response).object();
    const auto items = json.value("items").toArray();

    QList <User> user_list;
    for (const auto & item : items) {
        const auto user = item.toObject();
        user_list << user;
        m_user_list.append(user);
    }

    return user_list;
}

HttpResponse ErauUnbaParser::send_search_request(const int & limit, const int & offset)
{
    const auto query = build_search_query(limit, offset);
    QUrl url("http://erau.unba.org.ua/search");
    url.setQuery(query);

    const auto request = HttpUtils::browserRequest(url);
    const auto response = handle_response(QNetworkAccessManager::GetOperation, request);

    return response;
}

HttpResponse ErauUnbaParser::send_profile_request(const int &user_id)
{
    const auto path = QString("http://erau.unba.org.ua/profile/%1").arg(user_id);
    const auto request = HttpUtils::browserRequest(QUrl(path));
    const auto response = handle_response(QNetworkAccessManager::GetOperation, request);

    return response;
}

HttpResponse ErauUnbaParser::handle_response(const QNetworkAccessManager::Operation &op,
                                             const QNetworkRequest &request,
                                             const QByteArray &args)
{
    auto reply = (op == QNetworkAccessManager::GetOperation) ?
              m_network.get(request)
              : m_network.post(request, args);

    m_loop.exec();

    const auto response = HttpResponse::createFromReply(reply);
    reply->deleteLater();

    return response;
}
