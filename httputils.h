#ifndef HTTPUTILS_H
#define HTTPUTILS_H

#include <QNetworkRequest>
#include <QNetworkAccessManager>

class HttpUtils
{
public:
    HttpUtils();

    static QNetworkRequest browserRequest(const QUrl& url, const QNetworkAccessManager::Operation &op = QNetworkAccessManager::GetOperation);
};

#endif // HTTPUTILS_H
