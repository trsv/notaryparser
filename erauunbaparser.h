#ifndef ERAUUNBAPARSER_H
#define ERAUUNBAPARSER_H

#include "httpresponse.h"
#include "struct.h"

#include <QNetworkAccessManager>
#include <QEventLoop>
#include <QUrlQuery>

class ErauUnbaParser
{
public:
    ErauUnbaParser();
    ~ErauUnbaParser();

public:
    QList <User> parse();
    QList<Company> parse_additional_data(const QList <User> & user_list);

private:
    int get_user_list_size();
    int get_user_list_size_request();

    QUrlQuery build_search_query(const int & limit, const int & offset) const;

    QList <User> parse_user_list(const QByteArray & response);
    HttpResponse send_search_request(const int & limit, const int & offset);
    HttpResponse send_profile_request(const int & user_id);
    HttpResponse handle_response(const QNetworkAccessManager::Operation & op,
                                 const QNetworkRequest & request,
                                 const QByteArray & args = QByteArray());



private:
    const int m_USER_ON_PAGE_LIMIT;

    QNetworkAccessManager m_network;
    QEventLoop m_loop;

    QJsonArray m_user_list;
};

#endif // ERAUUNBAPARSER_H
