#ifndef ERNMINJUSTPARSER_H
#define ERNMINJUSTPARSER_H

#include "httpresponse.h"
#include "struct.h"

#include <QNetworkAccessManager>
#include <QEventLoop>
#include <QUrlQuery>

class ErnMinjustParser
{
public:
    ErnMinjustParser();
    ~ErnMinjustParser();

public:
    QList<Company> parse(const QMap <QString, QStringList> & list);
    QList<Company> parse(const QString & city, const QStringList & street_list);

private:
    QList <Company> parse_user_list(const QByteArray & response);

    QByteArray build_search_query(const QString & city, const QString & street);
    HttpResponse send_search_request(const QString & city, const QString & street);

//    HttpResponse send_profile_request(const int & user_id);
    HttpResponse handle_response(const QNetworkAccessManager::Operation & op,
                                 const QNetworkRequest & request,
                                 const QByteArray & args = QByteArray());

    QString get_aspnet_value(const QString & name);
    QMap <QString, QString> parse_city_list(const QByteArray & response);

    QList<Company> parsingAll(const QString& , const QString &);

private:
    QString RegexHiddenField_cvCertificateNum = "%5E%5B0-9%5D%7B0%2C5%7D%24";
    QString RegexHiddenField_cvFName = "%5E(%5B_%25%5D%7C.%7B1%7D%5B_%25%5D)%0A0";
    QString RegexHiddenField_cvName = "%5E(%5B_%25%5D%7C.%7B1%7D%5B_%25%5D)%0A0";
    QString RegexHiddenField_cvSName = "%5E(%5B_%25%5D%7C.%7B1%7D%5B_%25%5D)%0A0";
    QString __VIEWSTATEGENERATOR;
    QString __VIEWSTATE;
    QString ctl00DefaultContentASPxGridView1CallbackState;

    QString m_general_search_page;

    QNetworkAccessManager m_network;
    QEventLoop m_loop;

    QJsonArray m_user_list;
    QMap <QString, QString> m_city_list, m_area_list;
    QList<InfoStruct> m_last_info_list;
    ValueParser m_parser;
};

#endif // ERNMINJUSTPARSER_H
